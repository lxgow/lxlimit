package lxlimit

import (
	"fmt"
	redigo "github.com/garyburd/redigo/redis"
)

// redis
// 创建时间:2023/11/24 11:25
// 创建人:lixu

type MapRedisConfig struct {
	Host   string `json:"host"`
	Port   string `json:"port"`
	Pwd    string `json:"pwd"`
	Db     string `json:"Db"`
	Prefix string `json:"prefix"`
}

func getRedis() redigo.Conn {
	return LXLIMITCL.RedisPool.Get()
}

type LXredis struct {
	Name   string `json:"name"`
	Val    string `json:"val"`
	Db     string `json:"db"`
	Prefix string `json:"prefix"`
}

// initRedis 初始化Redis
func initRedis(config MapRedisConfig) (err error) {

	LXLIMITCL.RedisConfig = config

	pool_size := 20

	LXLIMITCL.RedisPool = redigo.NewPool(func() (redigo.Conn, error) {
		c, err := redigo.Dial("tcp", fmt.Sprintf("%s:%s", LXLIMITCL.RedisConfig.Host, LXLIMITCL.RedisConfig.Port))
		if err != nil {
			return nil, err
		}
		if _, err := c.Do("AUTH", LXLIMITCL.RedisConfig.Pwd); err != nil {
			c.Close()
			return nil, err
		}
		if _, err := c.Do("SELECT", LXLIMITCL.RedisConfig.Db); err != nil {
			c.Close()
			return nil, err
		}

		return c, nil
	}, pool_size)

	return
}

// SetTime 设置redis字符串 加过期时间
func (con *LXredis) setTime(times int) (err error) {

	var nname string

	if con.Prefix != "" {
		nname = con.Prefix + con.Name
	} else {
		nname = LXLIMITCL.RedisConfig.Prefix + con.Name
	}

	redis := getRedis()

	defer redis.Close()

	if con.Db != "" {
		if _, err = redis.Do("SELECT", con.Db); err != nil {
			return
		}
	}

	_, err = redis.Do("set", nname, con.Val, "EX", times)

	return
}

// GET 获取redis
func (con *LXredis) GET() (gstr string, err error) {

	var nname string

	if con.Prefix != "" {
		nname = con.Prefix + con.Name
	} else {
		nname = LXLIMITCL.RedisConfig.Prefix + con.Name
	}

	redis := getRedis()

	defer redis.Close()

	if con.Db != "" {
		if _, err = redis.Do("SELECT", con.Db); err != nil {
			return
		}

	}

	gstr, err = redigo.String(redis.Do("GET", nname))

	return
}

// IsExist 判断key是否存在
func (con *LXredis) IsExist() (isexit bool, err error) {

	var nname string

	if con.Prefix != "" {
		nname = con.Prefix + con.Name
	} else {
		nname = LXLIMITCL.RedisConfig.Prefix + con.Name
	}

	redis := getRedis()

	defer redis.Close()

	if con.Db != "" {
		if _, err = redis.Do("SELECT", con.Db); err != nil {
			return
		}
	}

	isexit, err = redigo.Bool(redis.Do("EXISTS", nname))
	return
}

// INCR 自增
func (con *LXredis) INCR() (nums int64, err error) {

	var nname string

	if con.Prefix != "" {
		nname = con.Prefix + con.Name
	} else {
		nname = LXLIMITCL.RedisConfig.Prefix + con.Name
	}

	redis := getRedis()

	defer redis.Close()

	if con.Db != "" {
		if _, err = redis.Do("SELECT", con.Db); err != nil {
			return
		}
	}

	nums, err = redigo.Int64(redis.Do("INCR", nname))

	return
}

// TTL 查看过期时间
func (con *LXredis) TTL() (ttl int, err error) {

	var nname string

	if con.Prefix != "" {
		nname = con.Prefix + con.Name
	} else {
		nname = LXLIMITCL.RedisConfig.Prefix + con.Name
	}

	redis := getRedis()

	defer redis.Close()

	if con.Db != "" {
		if _, err = redis.Do("SELECT", con.Db); err != nil {
			return
		}
	}

	ttl, err = redigo.Int(redis.Do("ttl", nname))

	return
}

// EXPIRE 设置过期时间
func (con *LXredis) EXPIRE(times int) (err error) {

	var nname string

	if con.Prefix != "" {
		nname = con.Prefix + con.Name
	} else {
		nname = LXLIMITCL.RedisConfig.Prefix + con.Name
	}

	redis := getRedis()

	defer redis.Close()

	if con.Db != "" {
		if _, err = redis.Do("SELECT", con.Db); err != nil {
			return
		}
	}

	_, err = redigo.Int(redis.Do("EXPIRE", nname, times))

	return
}
